import re

login = """{"op":"Login","data":{"username_or_email":"gwordcountbot","password":"you_thought"}}"""

get_messages = """{{"op":"GetUserMentions","data":{{"sort":"New","unread_only":true,"page":1,"limit":40,"auth":"{auth}"}}}}"""

mark_as_read = """{{"op":"MarkUserMentionAsRead","data":{{"user_mention_id":{user_mention_id},"read":true,"auth":"{auth}"}}}}"""

create_comment = """{{"op":"CreateComment","data":{{"auth":"{auth}","content":"{content}","post_id":{post_id},"creator_id":{creator_id},"parent_id":{parent_id}}}}}"""

get_user_content = """{{"op":"GetUserDetails","data":{{"user_id":null,"username":"{username}","sort":"TopAll","saved_only":false,"page":{page},"limit":100,"auth":"{auth}"}}}}"""
g_words = re.compile("g.mer")
g_words_hard = re.compile("gamer")
