import asyncio
import json
import time

import websockets

from .constants import (
    create_comment,
    g_words,
    g_words_hard,
    get_messages,
    get_user_content,
    login,
    mark_as_read,
)


def format_response(response: str) -> dict:
    response = json.loads(response)
    return response


async def check_and_comment():
    uri = "wss://www.hexbear.net/api/v1/ws"
    async with websockets.connect(uri) as websocket:
        # first login to get auth
        await websocket.send(login)
        login_info = await websocket.recv()
        login_info = format_response(login_info)
        auth = login_info["data"]["jwt"]

        # then get user info on self
        user_self = "gwordcountbot"
        page = 1
        await websocket.send(
            get_user_content.format(username=user_self, auth=auth, page=page)
        )
        self_user_content = await websocket.recv()
        self_user_content = format_response(self_user_content)
        creator_id = self_user_content["data"]["user"]["id"]

        # check unread mentions
        await websocket.send(get_messages.format(auth=auth))
        unread_mentions = await websocket.recv()
        unread_mentions = format_response(unread_mentions)

        # loop through mentions and do the count
        for mention in unread_mentions["data"]["mentions"]:
            page = 1
            # get mention stats
            mention_id = mention["id"]
            post_id = mention["post_id"]
            user_mention_id = mention["user_mention_id"]

            # set g_word_usage to 0
            g_word_usage = 0
            g_word_hard = 0

            # grab full text content of user comments
            try:
                username = (
                    mention["content"]
                    .split()[1]
                    .split("[")[1]
                    .split("]")[0]
                    .replace("@", "")
                    .replace("www.chapo.chat","")
                )
            except IndexError:
                comment_content = "Sorry товарищ, that's not the right format. I need you to ping my username and then immediately follow it with the username you want to be checked."
                await websocket.send(
                    create_comment.format(
                        auth=auth,
                        content=comment_content,
                        parent_id=mention_id,
                        post_id=post_id,
                        creator_id=creator_id,
                    )
                )
                comment_posted = await websocket.recv()
                await websocket.send(
                    mark_as_read.format(auth=auth, user_mention_id=user_mention_id)
                )
                marked_as_read = await websocket.recv()
                continue

            paginated = False
            full_content = ""
            while paginated is False:
                time.sleep(2)
                await websocket.send(
                    get_user_content.format(username=username, auth=auth, page=page)
                )
                user_full_comments = await websocket.recv()
                user_full_comments = format_response(user_full_comments)

                # put all comments into one
                try:
                    for comment in user_full_comments["data"]["comments"]:
                        full_content += " " + comment["content"]
                    if len(user_full_comments["data"]["comments"]) >= 100:
                        page += 1
                        continue
                    else:
                        paginated = True
                except KeyError:
                    if user_full_comments.get("error"):
                        time.sleep(15)
                        continue
            full_content = full_content.lower()
            g_word_usage = len(g_words.findall(full_content))
            g_word_hard = len(g_words_hard.findall(full_content))

            if g_word_usage == 0:
                comment_content = "Thank you for the request, товарищ. {username} has not said the G-word yet.".format(
                    username=username
                )
            elif g_word_usage == 1 and g_word_hard == 0:
                comment_content = "Thank you for the request, товарищ. {username} has said the G-word only once, taking care to use its censored form.".format(
                    username=username
                )
            elif g_word_usage == 1 and g_word_hard == 1:
                comment_content = "Thank you for the request, товарищ. {username} has said the G-word only once, using its uncensored version.".format(
                    username=username
                )
            elif g_word_usage > 1 and g_word_hard == 1:
                comment_content = "Thank you for the request, товарищ. {username} has said the G-word {g_word_usage} times, using its uncensored version only once.".format(
                    username=username,
                    g_word_usage=g_word_usage,
                    g_word_hard=g_word_hard,
                )
            elif g_word_usage > 1 and g_word_hard > 1:
                comment_content = "Thank you for the request, товарищ. {username} has said the G-word {g_word_usage} times, using its uncensored version {g_word_hard} times.".format(
                    username=username,
                    g_word_usage=g_word_usage,
                    g_word_hard=g_word_hard,
                )
            else:
                comment_content = "Thank you for the request, товарищ. {username} has said the G-word {g_word_usage} times, always taking care to use its censored form.".format(
                    username=username, g_word_usage=g_word_usage
                )
            await websocket.send(
                create_comment.format(
                    auth=auth,
                    content=comment_content,
                    parent_id=mention_id,
                    post_id=post_id,
                    creator_id=creator_id,
                )
            )
            comment_posted = await websocket.recv()
            comment_posted = format_response(comment_posted)
            await websocket.send(
                mark_as_read.format(auth=auth, user_mention_id=user_mention_id)
            )
            marked_as_read = await websocket.recv()
        return


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(check_and_comment())
    asyncio.get_event_loop().close()
